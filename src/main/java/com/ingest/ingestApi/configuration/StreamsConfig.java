package com.ingest.ingestApi.configuration;

import com.ingest.ingestApi.streams.IngestFileUploadedStream;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(IngestFileUploadedStream.class)
public class StreamsConfig {
}
