package com.ingest.ingestApi.models.receipts;

import lombok.*;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Getter
@Setter
@AllArgsConstructor
public class DatastoreReceipt extends Receipt {
    @Builder
    public DatastoreReceipt(UUID transactionId, String vendorName, String dataSet, String fileFormat, long numberOfRecords, ReceiptType receiptType) {
        super(transactionId);
        this.vendorName = vendorName;
        this.dataSet = dataSet;
        this.fileFormat = fileFormat;
        this.numberOfRecords = numberOfRecords;
        this.receiptType = receiptType;
    }

    private String vendorName;
    private String dataSet;
    private String fileFormat;
    private long numberOfRecords;
    private ReceiptType receiptType;
}
