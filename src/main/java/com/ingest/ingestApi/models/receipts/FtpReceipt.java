package com.ingest.ingestApi.models.receipts;

import lombok.*;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Value
@Builder
@Getter
@Setter
@AllArgsConstructor
public class FtpReceipt extends Receipt {
    private String ftpPath;
    private String localPath;
    private String tempStoragePath;
    private String sourceIp;
    private String vendorName;
    private String dataSet;
    private long uploadDate;
    private String fileFormat;
    private long fileSizeBytes;
    private String fileName;
    private ReceiptType receiptType;

    public FtpReceipt(UUID transactionId, String ftpPath, String localPath, String tempStoragePath, String sourceIp, String vendorName,
                      String dataSet, long uploadDate, String fileFormat, long fileSizeBytes, String fileName, ReceiptType receiptType) {
        super(transactionId);
        this.ftpPath = ftpPath;
        this.localPath = localPath;
        this.tempStoragePath = tempStoragePath;
        this.sourceIp = sourceIp;
        this.vendorName = vendorName;
        this.dataSet = dataSet;
        this.uploadDate = uploadDate;
        this.fileFormat = fileFormat;
        this.fileSizeBytes = fileSizeBytes;
        this.fileName = fileName;
        this.receiptType = receiptType;
    }
}
