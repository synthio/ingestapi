package com.ingest.ingestApi.models.receipts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public abstract class Receipt {
    private UUID transactionId;
}
