package com.ingest.ingestApi.models.receipts;

import lombok.*;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
public class TempStorageReceipt extends Receipt {

    @Builder
    public TempStorageReceipt(UUID transactionId, String bucket, String key, long ftpToTempStorageInMilliseconds, String vendorName, String dataSet, ReceiptType receiptType) {
        super(transactionId);
        this.bucket = bucket;
        this.key = key;
        this.ftpToTempStorageInMilliseconds = ftpToTempStorageInMilliseconds;
        this.vendorName = vendorName;
        this.dataSet = dataSet;
        this.receiptType = receiptType;
    }

    private String bucket;
    private String key;
    private long ftpToTempStorageInMilliseconds;
    private String vendorName;
    private String dataSet;
    private ReceiptType receiptType;
}
