package com.ingest.ingestApi.models.receipts;

import lombok.*;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
public class ArchiveStorageReceipt extends Receipt {
    @Builder
    public ArchiveStorageReceipt(UUID transactionId, String vendorName, String dataSet,
                                 String fileFormat, long fileSizeBytes, String fileName, ReceiptType receiptType, long tempToArchiveInMilliseconds) {
        super(transactionId);
        this.vendorName = vendorName;
        this.dataSet = dataSet;
        this.fileFormat = fileFormat;
        this.fileSizeBytes = fileSizeBytes;
        this.fileName = fileName;
        this.receiptType = receiptType;
        this.tempToArchiveInMilliseconds = tempToArchiveInMilliseconds;
    }

    private String vendorName;
    private String dataSet;
    private String fileFormat;
    private long fileSizeBytes;
    private String fileName;
    private ReceiptType receiptType;
    private long tempToArchiveInMilliseconds;
}
