package com.ingest.ingestApi.models.receipts;

public enum ReceiptType {
    Ftp,
    TempStorage,
    ArchiveStorage,
    Datastore
}
