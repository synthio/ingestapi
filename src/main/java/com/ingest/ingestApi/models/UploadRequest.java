package com.ingest.ingestApi.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class UploadRequest {

    private String ftpPath;
    private String sourceIp;
    private String vendorName;
    private String uploadDate;
    private String dataSet;
    private UUID transactionId;

    public UploadRequest() {

    }

    public UploadRequest(String ftpPath, String sourceIp, String vendorName, String uploadDate, String dataSet, UUID transactionId) {
        this.ftpPath = ftpPath;
        this.sourceIp = sourceIp;
        this.vendorName = vendorName;
        this.uploadDate = uploadDate;
        this.dataSet = dataSet;
        this.transactionId = transactionId;
    }

    @JsonProperty("transactionId")
    public UUID getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }

    @JsonProperty("ftpPath")
    public String getFtpPath() {
        return ftpPath;
    }

    public void setFtpPath(String ftpPath) {
        this.ftpPath = ftpPath;
    }


    @JsonProperty("vendorName")
    public String getVendorName() {
        return this.vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @JsonProperty("sourceIp")
    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    @JsonProperty("uploadDate")
    public String getUploadDate() {
        return this.uploadDate;
    }

    public void setUploadDate(String uploadDate) {

        this.uploadDate = uploadDate;
    }

    @JsonProperty("dataSet")
    public String getDataSet() {
        return this.dataSet;
    }

    public void setDataSet(String dataSet) { this.dataSet = dataSet; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UploadRequest that = (UploadRequest) o;
        return Objects.equals(ftpPath, that.ftpPath) &&
                Objects.equals(sourceIp, that.sourceIp) &&
                Objects.equals(vendorName, that.vendorName) &&
                Objects.equals(uploadDate, that.uploadDate) &&
                Objects.equals(dataSet, that.dataSet) &&
                Objects.equals(transactionId, that.transactionId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(ftpPath, sourceIp, vendorName, uploadDate, dataSet, transactionId);
    }

    @Override
    public String toString() {
        return "FtpUploadRequest{" +
                "ftpPath='" + ftpPath + '\'' +
                ", sourceIp='" + sourceIp + '\'' +
                ", vendorName='" + vendorName + '\'' +
                ", uploadDate='" + uploadDate + '\'' +
                ", dataSet='" + dataSet + '\'' +
                ", transactionId=" + transactionId +
                '}';
    }
}
