package com.ingest.ingestApi.controllers;

import com.ingest.ingestApi.models.UploadRequest;
import com.ingest.ingestApi.models.receipts.FtpReceipt;
import com.ingest.ingestApi.models.receipts.ReceiptType;
import com.ingest.ingestApi.streams.IngestFileUploadedStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;

@RestController
public class ThirdpartyIngestController {
    @Autowired
    private IngestFileUploadedStream ingestFileUploadedStream;

    @PostMapping("/vendor")
    public Mono<String> vendorUpload(@RequestBody UploadRequest upload) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");

        FtpReceipt ftpReceipt = FtpReceipt.builder()
                .vendorName(upload.getVendorName())
                .dataSet(upload.getDataSet())
                .sourceIp(upload.getSourceIp())
                .ftpPath(upload.getFtpPath())
                .receiptType(ReceiptType.Ftp)
                .fileSizeBytes(0)
                .uploadDate(formatter.parse(upload.getUploadDate()).getTime()).build();
        ftpReceipt.setTransactionId(UUID.randomUUID());
        ingestFileUploadedStream.outboundRecords().send(MessageBuilder.withPayload(ftpReceipt)
                .build());
        return Mono.just(ftpReceipt.toString());
    }
}
