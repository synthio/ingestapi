package com.ingest.ingestApi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.WebFilter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;

@SpringBootApplication
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = "com.ingest.ingestApi")
@EnableSwagger2WebFlux
@EnableAutoConfiguration
public class IngestApiApplication {
	@Autowired
	private org.springframework.core.env.Environment env;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping(env.getProperty("base.path"))
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

	@Bean
	public WebFilter contextPathWebFilter() {
		String contextPath = env.getProperty("base.path");
		return (exchange, chain) -> {
			ServerHttpRequest request = exchange.getRequest();
			if (request.getURI().getPath().startsWith(contextPath)) {
				return chain.filter(
						exchange.mutate()
								.request(request.mutate().contextPath(contextPath).build())
								.build());
			}
			return chain.filter(
			        exchange.mutate().request(
			                request.mutate().path("/notFound").build())
                            .build());
		};
	}

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(IngestApiApplication.class);
		application.run(args);
	}
}
