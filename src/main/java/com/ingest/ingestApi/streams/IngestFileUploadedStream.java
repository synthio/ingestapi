package com.ingest.ingestApi.streams;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public interface IngestFileUploadedStream {
    String OUTPUT = "ingestFileUploaded-out";

    @Output(OUTPUT)
    MessageChannel outboundRecords();
}
